import React, {useState} from 'react';

export const List = () => {
  // const [list, setList] = useState(['jh', 'utu', 'rert', 'oiu', 'ghj']);
  const [list, setList] = useState([Math.random()]);

  const addItem = () => {
    setList(prev => [...prev, Math.random()]
    )
  }

  const removeItem = () => {
    setList(prev => {
      // prev.splice(prev.length-1, 1);
      prev.pop();
      console.log(prev);
      return [...prev];
    })
  }

  const deleteItem = (i=list.length-1) => {
    setList(prev => prev.filter((_, ind) => i !== ind))
  }

  return (
    <div>
      <button onClick={addItem}>Add Item</button>
      <ul>
      {list.map((item, index) => <li onClick={() => deleteItem(index)} key={index}>{item}</li>)}
      </ul>
      {/* <button onClick={() => deleteItem()}>Remove Item</button> */}
      <button onClick={() => removeItem()}>Remove Item</button>
    </div>
  );
}