import React, {useState} from 'react';

export const Counter = ({step=1, start=0}) => {
  const [counter, setCounter] = useState(start)
  
  let clickHandlerPlus = () => setCounter(n => n + step);

  let clickHandlerMinus = () => setCounter(n => n - step);

  let clickHandlerReset = () => setCounter(start);

  return (<div>
    <h1>{counter}</h1>
    <button onClick={clickHandlerMinus}>-</button>
    <button onClick={clickHandlerReset}>Reset</button>
    <button onClick={clickHandlerPlus}>+</button>
  </div>)
}