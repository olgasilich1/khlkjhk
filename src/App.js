// import logo from './logo.svg';
import './App.css';
import {Counter} from './components/Counter';
import {List} from './components/List'

function App() {
  return (
    <div className="App">
      {/* <Counter step={5} start={10} />
      <Counter /> */}
      <List />
    </div>
  );
}

export default App;
